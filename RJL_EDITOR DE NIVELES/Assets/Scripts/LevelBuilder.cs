﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class LevelElement //define cada objeto en el nivel con los caracteres utilizados a prefab
{
    public string m_Character;
    public GameObject m_Prefab;
}
public class LevelBuilder : MonoBehaviour
{
    public GameObject groundUnder;
    public int m_CurrentLevel;
    public List<LevelElement> m_LevelElements;
    private Level m_Level;
    GameObject GetPrefab(char c) //mira todos los elementos
    {
        LevelElement levelElement = m_LevelElements.Find(le => le.m_Character == c.ToString());
        if(levelElement!=null)
        {
            return levelElement.m_Prefab;
            
        }
        else
        {
            return null;
        }
    }
    public void NextLevel()
    {
        m_CurrentLevel++;
        if(m_CurrentLevel>=GetComponent<Levels>().m_Levels.Count)
        {
            m_CurrentLevel = 0;//Vamos al nivel 1 si hemos completado todos los niveles disponibles como un bucle

        }
    }
    public void Build()//el mas importante para construir el nivel
    {
        m_Level = GetComponent<Levels>().m_Levels[m_CurrentLevel];
        //hacemos que enpiece todo en el centro de cordenadas
        int startx = -m_Level.Width / 2;// para hacer el centro la pantalla el centro del nivel
        int x = startx;
        int y = -m_Level.Height / 2;
        foreach(var row in m_Level.m_Rows)
        {
            foreach(var ch in row)
            {
                Debug.Log(ch);
                GameObject prefab = GetPrefab(ch);
                if(prefab)
                {
                    Debug.Log(prefab.name);
                    Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity);



                    if (ch.Equals('@') || ch.Equals('$'))
                    {
                        Instantiate(groundUnder, new Vector3(x, y, 0), Quaternion.identity);
                    }
                  
                   
                }
                x++;
            }
            y++;
            x = startx;
        }

    }

	
}
