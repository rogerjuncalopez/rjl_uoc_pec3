﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakerTile : MonoBehaviour
{

    public Sprite sprite;
    public int id;// change for string 
    public float x;
    public float y;
    public string character;

    private void Start()
    {
        x = gameObject.transform.position.x;
        y = gameObject.transform.position.y;
    }
}
